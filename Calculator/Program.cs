﻿using System;
using CalculateInterest.Builders;
using CalculateInterest.Configuration;
using CalculateInterest.Services;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculationConfigurationProvider = new InterestCalculationConfigurationProvider();
            var builder = new CurrencyViewModelBuilder(new CurrencyFormatter(calculationConfigurationProvider), new InterestService(calculationConfigurationProvider));

            while (true)
            {
                Console.Out.WriteLine("-------------------------------------------");
                Console.Out.WriteLine("Give amount to calculate interest");
                var amount = Console.In.ReadLine();

                Console.Out.WriteLine($"Calculated interest: {builder.BuildInterests(amount)}");
            }
        }
    }
}
