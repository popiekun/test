﻿namespace CalculateInterest.Services.Interfaces
{
    public interface IInterestService
    {
        decimal GetInterests(decimal amount);
    }
}
