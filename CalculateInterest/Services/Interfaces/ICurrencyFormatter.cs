﻿namespace CalculateInterest.Services.Interfaces
{
    public interface ICurrencyFormatter
    {
        string GetFormattedCurrency(decimal amount);
    }
}
