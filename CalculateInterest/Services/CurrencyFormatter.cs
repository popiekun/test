﻿using CalculateInterest.Configuration.Interfaces;
using CalculateInterest.Services.Interfaces;
using System.Globalization;

namespace CalculateInterest.Services
{
    public class CurrencyFormatter : ICurrencyFormatter
    {
        private readonly IInterestCalculationConfigurationProvider _interestCalculationConfigurationProvider;

        public CurrencyFormatter(IInterestCalculationConfigurationProvider @object)
        {
            _interestCalculationConfigurationProvider = @object;
        }

        public string GetFormattedCurrency(decimal amount)
        {
            return FormatToCurrencyString(amount);
        }

        private string FormatToCurrencyString(decimal value)
        {
            return value.ToString(_interestCalculationConfigurationProvider.GetInterestConfiguration().StringFormat, CultureInfo.CreateSpecificCulture(_interestCalculationConfigurationProvider.GetInterestConfiguration().Culture));
        }
    }
}
