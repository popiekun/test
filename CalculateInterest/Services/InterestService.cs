﻿using System;
using System.Linq;
using CalculateInterest.Configuration.Interfaces;
using CalculateInterest.Services.Interfaces;

namespace CalculateInterest.Services
{
    public class InterestService : IInterestService
    {
        private readonly IInterestCalculationConfigurationProvider _interestCalculationConfigurationProvider;


        public InterestService(IInterestCalculationConfigurationProvider @object)
        {
            _interestCalculationConfigurationProvider = @object;
        }

        public decimal GetInterests(decimal amount)
        {
            if (amount < 0)
                return 0;

            var interestConfigurationItem =
                _interestCalculationConfigurationProvider.GetInterestConfiguration().InterestConfigurationItems.FirstOrDefault(x => (x.Min <= amount || x.Min == null) && (x.Max > amount || x.Max == null));

            if (interestConfigurationItem == null)
                throw new ApplicationException($"Missing configuration for amount {amount}");

            return (amount * interestConfigurationItem.InterestPercentage / 100);
        }
    }
}
