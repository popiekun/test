﻿namespace CalculateInterest.Configuration.Interfaces
{
    public interface IInterestCalculationConfigurationProvider
    {
        InterestConfiguration GetInterestConfiguration();
    }
}
