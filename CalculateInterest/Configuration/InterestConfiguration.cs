﻿namespace CalculateInterest.Configuration
{
    public class InterestConfiguration
    {
        public string Culture;
        public InterestConfigurationItem[] InterestConfigurationItems { get; set; }
        public string StringFormat { get; set; }
    }

    public class InterestConfigurationItem
    {
        public decimal? Max { get; set; }
        public decimal InterestPercentage { get; set; }
        public decimal? Min { get; set; }
    }
}
