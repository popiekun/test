﻿using CalculateInterest.Configuration.Interfaces;

namespace CalculateInterest.Configuration
{
    public class InterestCalculationConfigurationProvider : IInterestCalculationConfigurationProvider
    {
        public InterestConfiguration GetInterestConfiguration()
        {
            var configuration = new InterestConfiguration()
            {
                Culture = "en-GB",
                StringFormat = "C2",
                InterestConfigurationItems = new[]
                {
                    new InterestConfigurationItem()
                    {
                        InterestPercentage = 1m,
                        Max = 1000m
                    },
                    new InterestConfigurationItem()
                    {
                        InterestPercentage = 1.5m,
                        Min = 1000m,
                        Max = 5000m
                    },
                    new InterestConfigurationItem()
                    {
                        InterestPercentage = 2m,
                        Min = 5000,
                        Max = 10000m
                    },
                    new InterestConfigurationItem()
                    {
                        InterestPercentage = 2.5m,
                        Min = 10000m,
                        Max = 50000m
                    },
                    new InterestConfigurationItem()
                    {
                        InterestPercentage = 3m,
                        Min = 50000m
                    }
                }
            };

            return configuration;
        }
    }
}
