﻿using CalculateInterest.Services.Interfaces;

namespace CalculateInterest.Builders
{
    public class CurrencyViewModelBuilder
    {
        private readonly ICurrencyFormatter _currencyFormatter;
        private readonly IInterestService _interestService;

        public CurrencyViewModelBuilder(ICurrencyFormatter currencyFormatter, IInterestService interestService)
        {
            _currencyFormatter = currencyFormatter;
            _interestService = interestService;
        }

        public string BuildInterests(string amount)
        {
            if (!decimal.TryParse(amount, out var decimalAmount))
                return "Incorrect amount value";

            return _currencyFormatter.GetFormattedCurrency(_interestService.GetInterests(decimalAmount));
        }
    }
}
