﻿using CalculateInterest.Configuration;
using CalculateInterest.Configuration.Interfaces;
using CalculateInterest.Services;
using CalculateInterest.Services.Interfaces;
using Moq;
using NUnit.Framework;

namespace CalculateInterests.Tests.Services
{
    [TestFixture]
    public class InterestServiceTests
    {
        private Mock<IInterestCalculationConfigurationProvider> _interestCalculationConfigurationProviderMock;

        private IInterestService _service;


        [SetUp]
        public void SetUp()
        {
            _interestCalculationConfigurationProviderMock = new Mock<IInterestCalculationConfigurationProvider>();
        }

        [TestCase(-1, 0.00)]
        [TestCase(0, 0.00)]
        [TestCase(500, 5.00)]
        [TestCase(999.99999, 9.9999999)]
        [TestCase(1001, 15.015)]
        [TestCase(5000, 100.00)]
        [TestCase(10000, 250.00)]
        [TestCase(50000, 1500.00)]
        public void GetFormatedInterestValue_ProvidedAmount_ReturnsExpectedInterests(decimal amount, decimal expected)
        {
            // Arrange
            _interestCalculationConfigurationProviderMock.Setup(x => x.GetInterestConfiguration()).Returns(
                new InterestConfiguration()
                {
                    InterestConfigurationItems = new[]
                    {
                        new InterestConfigurationItem()
                        {
                            InterestPercentage = 1m,
                            Max = 1000m
                        },
                        new InterestConfigurationItem()
                        {
                            InterestPercentage = 1.5m,
                            Min = 1000m,
                            Max = 5000m
                        },
                        new InterestConfigurationItem()
                        {
                            InterestPercentage = 2m,
                            Min = 5000,
                            Max = 10000m
                        },
                        new InterestConfigurationItem()
                        {
                            InterestPercentage = 2.5m,
                            Min = 10000m,
                            Max = 50000m
                        },
                        new InterestConfigurationItem()
                        {
                            InterestPercentage = 3m,
                            Min = 50000m
                        }
                    }
                });
            _service = new InterestService(_interestCalculationConfigurationProviderMock.Object);

            // Act
            var result = _service.GetInterests(amount);

            // Assert
            Assert.AreEqual(expected, result);
        }
    }
}
