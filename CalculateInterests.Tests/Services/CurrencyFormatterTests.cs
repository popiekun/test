﻿using CalculateInterest.Configuration;
using CalculateInterest.Configuration.Interfaces;
using CalculateInterest.Services;
using CalculateInterest.Services.Interfaces;
using Moq;
using NUnit.Framework;

namespace CalculateInterests.Tests.Services
{
    [TestFixture]
    public class CurrencyFormatterTests
    {
        private Mock<IInterestCalculationConfigurationProvider> _interestCalculationConfigurationProviderMock;

        private ICurrencyFormatter _service;

        [SetUp]
        public void SetUp()
        {
            _interestCalculationConfigurationProviderMock = new Mock<IInterestCalculationConfigurationProvider>();
        }

        [TestCase(0, "£0.00")]
        [TestCase(15.02, "£15.02")]
        [TestCase(100, "£100.00")]
        [TestCase(250, "£250.00")]
        [TestCase(1500, "£1,500.00")]
        public void GetFormattedCurrency_GivenAmount_returnsFormattedString(decimal amount, string expected)
        {
            // Arrange
            _interestCalculationConfigurationProviderMock.Setup(x => x.GetInterestConfiguration()).Returns(
                new InterestConfiguration()
                {
                    Culture = "en-GB",
                    StringFormat = "C2"
                }
                );
            _service = new CurrencyFormatter(_interestCalculationConfigurationProviderMock.Object);

            // Act
            var result = _service.GetFormattedCurrency(amount);

            // Assert
            Assert.AreEqual(expected, result);
        }
    }
}
